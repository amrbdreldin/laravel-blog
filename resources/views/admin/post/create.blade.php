@extends('layouts.main')

@section('content')
    <section class="blog-area section">
        <div class="container">
    <div class="row">
        <div class="col-md-12 post-wrapper">

            @if($errors->any())
                <div class="col-sm-12">
                    <div class="card">
                        <div class="alert alert-danger">
                            {{__('Some fields are invalid please fix them')}}
                        </div>
                    </div>
                </div>
            @elseif(Session::has('status'))
                <div class="col-sm-12">
                    <div class="card">
                        <div class="alert alert-{{Session::get('status')}}">
                            {{ Session::get('msg') }}
                        </div>
                    </div>
                </div>
            @endif
            {!! Form::open(['route' => isset($data->id) ? ['admin.post.update',$data->id]:'admin.post.store','method' => isset($data->id) ?  'PATCH' : 'POST','files' => true]) !!}

                <div class="form-group col-sm-12{!! formError($errors,'category_id',true) !!}">
                    <div class="controls">
                        {!! Form::label('category_id','Category') !!}
                        {!! Form::select('category_id',[''=>'Select Category']+array_column($categories->toArray(),'title','id'),isset($data->id) ? $data->category_id:old('category_id'),['class'=>'form-control']) !!}
                    </div>
                    {!! formError($errors,'category_id') !!}
                </div>


                <div class="form-group col-sm-12{!! formError($errors,'title',true) !!}">
                    <div class="controls">
                        {!! Form::label('title','Title') !!}
                        {!! Form::text('title',isset($data->id) ? $data->title:old('title'),['class'=>'form-control']) !!}
                    </div>
                    {!! formError($errors,'title') !!}
                </div>

                <div class="form-group col-sm-12{!! formError($errors,'image',true) !!}">
                    <div class="controls">
                        {!! Form::label('image','Image') !!}
                        {!! Form::file('image',['class'=>'form-control']) !!}
                    </div>
                    {!! formError($errors,'image') !!}
                </div>

                @if(isset($data->id))
                    <div class="form-group col-sm-12">
                        <img style="width: 60px;height: 60px" src="{{asset('uploads/post/'.$data->image)}}" alt="">
                    </div>
                @endif

                <div class="form-group col-sm-12{!! formError($errors,'content',true) !!}">
                    <div class="controls">
                        {!! Form::label('content','Content') !!}
                        {!! Form::textarea('content',isset($data->id) ? $data->content:old('content'),['class'=>'form-control']) !!}
                    </div>
                    {!! formError($errors,'content') !!}
                </div>






                {!! Form::submit(__('Save'),['class'=>'btn btn-success pull-right']) !!}
                {!! Form::close() !!}


        </div>
    </div><!-- row -->
        </div><!-- container -->
    </section><!-- section -->
@endsection
