@extends('layouts.main')

@section('content')
    <section class="blog-area section">
        <div class="container">
    <div class="row">
        <div class="col-md-12 post-wrapper">
            <a href="{{route('admin.post.create')}}" class="btn btn-primary mb-2">Add New</a>
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Category</th>
                    <th scope="col">Title</th>
                    <th scope="col">Image</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
                </thead>
                <tbody>
                @if($data->isNotEmpty())
                    @foreach($data as $row)
                        <tr>
                            <th scope="row">{{$row->id}}</th>
                            <th scope="row">{{$row->category->title}}</th>
                            <td>{{$row->title}}</td>
                            <td><img src="{{asset('uploads/post/'.$row->image)}}" style="width: 60px;height: 60px" /></td>
                            <td>{{$row->user->name}}</td>
                            <td><a href="{{route('admin.post.edit',$row->id)}}" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
                            <td><a href="javascript:;" onclick="deleteRecord('{{route('admin.post.destroy',$row->id)}}')" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>


                        </tr>
                    @endforeach
                @else
                <tr><td colspan="4">There is no Data</td></tr>
                @endif
                </tbody>
            </table>
            {{ $data->links() }}

        </div>
    </div><!-- row -->
        </div><!-- container -->
    </section><!-- section -->
@endsection
