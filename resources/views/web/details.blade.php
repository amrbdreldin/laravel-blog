@extends('layouts.main')
@section('header')
    <link href="{{asset('assets/single-post-2/css/styles.css')}}" rel="stylesheet">

    <link href="{{asset('assets/single-post-2/css/responsive.css')}}" rel="stylesheet">
@endsection
@section('content')

    <div class="slider">

    </div><!-- slider -->

    <section class="post-area">
        <div class="container">

            <div class="row">

                <div class="col-lg-1 col-md-0"></div>
                <div class="col-lg-10 col-md-12">

                    <div class="main-post">

                        <div class="post-top-area">

                            <h5 class="pre-title">{{$data->category->title}}</h5>

                            <h3 class="title"><a><b>{{$data->title}}</b></a></h3>

                            <div class="post-info">

                                <div class="left-area">
                                    <a class="avatar"><img src="{{asset('user.svg')}}" alt="Admin"></a>
                                </div>

                                <div class="middle-area">
                                    <a class="name"><b>Admin</b></a>
                                    <h6 class="date">{{$data->created_at}}</h6>
                                </div>

                            </div><!-- post-info -->


                        </div><!-- post-top-area -->

                        <div class="post-image"><img src="{{asset('uploads/post/'.$data->image)}}"
                                                     alt="{{$data->title}}"></div>

                        <div class="post-bottom-area">

                            <p class="para">{{$data->content}}</p>

                            <div class="post-icons-area">
                                <ul class="post-icons">

                                    <li><a><i class="ion-chatbubble"></i>{{$data->comments()->count()}}</a></li>
                                    <li><a><i class="ion-eye"></i>{{$data->views}}</a></li>
                                </ul>

                                <ul class="icons">
                                    <li>SHARE :</li>
                                    <li><a><i class="ion-social-facebook"></i></a></li>
                                    <li><a><i class="ion-social-twitter"></i></a></li>
                                    <li><a><i class="ion-social-pinterest"></i></a></li>
                                </ul>
                            </div>

                            <div class="post-footer post-info">

                                <div class="left-area">
                                    <a class="avatar"><img src="{{asset('user.svg')}}" alt="Admin"></a>
                                </div>

                                <div class="middle-area">
                                    <a class="name"><b>Admin</b></a>
                                    <h6 class="date">{{$data->created_at}}</h6>
                                </div>

                            </div><!-- post-info -->

                        </div><!-- post-bottom-area -->

                    </div><!-- main-post -->
                </div><!-- col-lg-8 col-md-12 -->
            </div><!-- row -->
        </div><!-- container -->
    </section><!-- post-area -->

    @if($related->isNotEmpty())
        <section class="recomended-area section">
            <div class="container">
                <div class="row">
                    @foreach($related as $row)
                        <div class="col-lg-4 col-md-6">
                            <div class="card h-100">
                                <div class="single-post post-style-1">

                                    <div class="blog-image"><img src="{{asset('uploads/post/'.$row->image)}}"
                                                                 alt="{{$row->title}}"></div>

                                    <a class="avatar"><img src="{{asset('user.svg')}}"
                                                           alt="Admin"></a>

                                    <div class="blog-info">

                                        <h4 class="title"><a
                                                    href="{{route('details',['slug'=>$row->slug])}}"><b>{{$row->title}}</b></a>
                                        </h4>

                                        <ul class="post-footer">
                                            <li><a><i class="ion-chatbubble"></i>{{$row->comments()->count()}}</a></li>
                                            <li><a><i class="ion-eye"></i>{{$row->views}}</a></li>
                                        </ul>

                                    </div><!-- blog-info -->
                                </div><!-- single-post -->
                            </div><!-- card -->
                        </div><!-- col-lg-4 col-md-6 -->
                    @endforeach


                </div><!-- row -->

            </div><!-- container -->
        </section>
    @endif
    <section class="comment-section center-text">
        <div class="container">
            <h4><b>POST COMMENTS</b></h4>
            <div class="row">

                <div class="col-lg-2 col-md-0"></div>

                <div class="col-lg-8 col-md-12">
                    <div class="comment-form">
                        <form method="post" id="comment_form">
                            <div class="row">

                                <div class="col-sm-6">
                                    <input type="text" aria-required="true" name="contact-form-name"
                                           class="form-control"
                                           placeholder="Enter your name" aria-invalid="true" id="name" required>
                                </div><!-- col-sm-6 -->


                                <div class="col-sm-12">
									<textarea name="contact-form-message" rows="2" class="text-area-messge form-control"
                                              placeholder="Enter your comment" aria-required="true" id="comment"
                                              aria-invalid="false"></textarea>
                                </div><!-- col-sm-12 -->
                                <div class="col-sm-12">
                                    <button class="submit-btn" type="submit" id="form-submit"><b>POST COMMENT</b>
                                    </button>
                                </div><!-- col-sm-12 -->

                            </div><!-- row -->
                        </form>
                    </div><!-- comment-form -->

                    <h4><b>COMMENTS({{$comments_count}})</b></h4>
                    <div class="comments-here">
                        @if($comments->isNotEmpty())
                            @foreach($comments as $row)
                                <div class="commnets-area text-left">

                                    <div class="comment">

                                        <div class="post-info">

                                            <div class="left-area">
                                                <a class="avatar"><img src="{{asset('user2.png')}}" alt="user"></a>
                                            </div>
                                            <div class="right-area">
                                                <h6 class="date">{{$row->created_at}}</h6>
                                            </div>
                                            <div class="middle-area">
                                                <a class="name"><b>{{$row->name}}</b></a>

                                            </div>


                                        </div><!-- post-info -->

                                        <p>{{$row->comment}}</p>

                                    </div>

                                </div><!-- commnets-area -->
                            @endforeach
                    </div>
                    {{--<a class="more-comment-btn" href="#"><b>VIEW MORE COMMENTS</a>--}}
                    @endif
                </div><!-- col-lg-8 col-md-12 -->

            </div><!-- row -->

        </div><!-- container -->
    </section>


    <div class="comment_temp" style="display: none" >
        <div class="commnets-area text-left">

            <div class="comment">

                <div class="post-info">

                    <div class="left-area">
                        <a class="avatar"><img src="{{asset('user2.png')}}" alt="user"></a>
                    </div>
                    <div class="right-area">
                        <h6 class="date">Just Now</h6>
                    </div>
                    <div class="middle-area">
                        <a class="name"><b class="name_here"></b></a>
                    </div>


                </div><!-- post-info -->

                <p class="comment_here"></p>

            </div>

        </div><!-- commnets-area -->
    </div>

@endsection

@section('footer')


    <script>
        $('#comment_form').submit(function (e) {
            e.preventDefault();
           // var data = $(this).serialize();
            var name = $(this).find('#name').val();
            var comment = $(this).find('#comment').val();
            $.post('{{route('comment')}}',{'name':name,'comment':comment,'post_id':'{{$data->id}}'},function(data){
                if(data.status == false){
                    toastr.error(data.msg, 'Error !', {"closeButton": true});
                }else{
                    var row = $('.comment_temp');
                    row.find('.name_here').html(name);
                    row.find('.comment_here').html(comment);
                    $('.comments-here').prepend(row.html());
                    $('#comment_form')[0].reset();
                    toastr.success(data.msg, 'Success !', {"closeButton": true});
                }
            })

        })

    </script>

@endsection