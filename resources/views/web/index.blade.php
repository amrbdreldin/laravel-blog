@extends('layouts.main')

@section('header')
    <link href="{{asset('assets/front-page-category/css/styles.css')}}" rel="stylesheet">

    <link href="{{asset('assets/front-page-category/css/responsive.css')}}" rel="stylesheet">
@endsection
@section('content')

    @if($categories->isNotEmpty())
        <div class="main-slider">
            <div class="swiper-container position-static" data-slide-effect="slide" data-autoheight="false"
                 data-swiper-speed="500" data-swiper-autoplay="10000" data-swiper-margin="0"
                 data-swiper-slides-per-view="4"
                 data-swiper-breakpoints="true" data-swiper-loop="true">
                <div class="swiper-wrapper">

                    @foreach($categories as $category)

                        <div style="height: 177px" class="swiper-slide">
                            <a class="slider-category" href="{{route('home',['slug'=>$category->slug])}}">
                                <div class="blog-image"><img style="height: 177px" src="{{asset('uploads/category/'.$category->image)}}"
                                                             alt="{{$category->title}}"></div>

                                <div class="category">
                                    <div class="display-table center-text">
                                        <div class="display-table-cell">
                                            <h3><b>{{$category->title}}</b></h3>
                                        </div>
                                    </div>
                                </div>

                            </a>
                        </div><!-- swiper-slide -->
@endforeach

                </div><!-- swiper-wrapper -->

            </div><!-- swiper-container -->

        </div><!-- slider -->
    @endif


    <section class="blog-area section">
        <div class="container">
            @if($posts->isNotEmpty())
                <div class="row">

                    @foreach($posts as $row)
                        <div class="col-lg-4 col-md-6">
                            <div class="card h-100">
                                <div class="single-post post-style-1">

                                    <div class="blog-image"><img src="{{asset('uploads/post/'.$row->image)}}"
                                                                 alt="{{$row->title}}"></div>

                                    <a class="avatar"><img src="{{asset('user.svg')}}"
                                                                    alt="Admin"></a>

                                    <div class="blog-info">

                                        <h4 class="title"><a href="{{route('details',['slug'=>$row->slug])}}"><b>{{$row->title}}</b></a></h4>

                                        <ul class="post-footer">
                                            <li><a ><i class="ion-chatbubble"></i>{{$row->comments()->count()}}</a></li>
                                            <li><a ><i class="ion-eye"></i>{{$row->views}}</a></li>
                                        </ul>

                                    </div><!-- blog-info -->
                                </div><!-- single-post -->
                            </div><!-- card -->
                        </div><!-- col-lg-4 col-md-6 -->
                    @endforeach


                </div><!-- row -->


            @else
                <h1>THERE IS NO POSTS</h1>
            @endif
                {{ $posts->links() }}
        </div><!-- container -->
    </section><!-- section -->
@endsection
