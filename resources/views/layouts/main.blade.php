<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Laravel Blog</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Font -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">


	<!-- Stylesheets -->
	<link href="{{asset('assets/common-css/bootstrap.css')}}" rel="stylesheet">
	<link href="{{asset('assets/common-css/swiper.css')}}" rel="stylesheet">
	<link href="{{asset('assets/common-css/ionicons.css')}}" rel="stylesheet">
	<link href="{{asset('assets/blank-static/css/styles.css')}}" rel="stylesheet">
	<link href="{{asset('assets/blank-static/css/responsive.css')}}" rel="stylesheet">

	@yield('header')
</head>
<body >

	<header>
		<div class="container-fluid position-relative no-side-padding">

			<a href="{{route('home')}}" class="logo"><img src="{{asset('assets/images/logo.png')}}" alt="Logo Image"></a>

			<div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="ion-navicon"></i></div>


			<ul class="main-menu visible-on-click" id="main-menu">
				@if(Auth::check())
				<li><a href="javascript:;">Hello  {{ Auth::user()->name }}</a></li>
				<li><a href="{{route('admin.category.index')}}">Categories</a></li>
				<li><a href="{{route('admin.post.index')}}">Posts</a></li>
				<li><a href="{{route('logout',Auth::user()->id)}}">Logout</a></li>

				@endif
			</ul><!-- main-menu -->

			<div class="src-area">
				<form method="get" action="{{route('home')}}" >
					<button class="src-btn" type="submit"><i class="ion-ios-search-strong"></i></button>
					<input name="word" class="src-input" type="text" placeholder="Type of search">
				</form>
			</div>


		</div><!-- conatiner -->
	</header>




			@yield('content')




	<footer>
		<div class="container">
			<div class="row">

				<div class="col-lg-12 col-md-6">
					<div class="footer-section">

						<a class="logo" href="#"><img src="{{asset('assets/images/logo.png')}}" alt="Logo Image"></a>
						<p class="copyright">Bona @ 2017. All rights reserved.</p>
						<p class="copyright">Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>

					</div><!-- footer-section -->
				</div><!-- col-lg-4 col-md-6 -->



			</div><!-- row -->
		</div><!-- container -->
	</footer>


	<!-- SCIPTS -->


	<script src="{{asset('assets/common-js/jquery-3.1.1.min.js')}}"></script>

	<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

	<script src="{{asset('assets/common-js/tether.min.js')}}"></script>

	<script src="{{asset('assets/common-js/bootstrap.js')}}"></script>

	<script src="{{asset('assets/common-js/swiper.js')}}"></script>

	<script src="{{asset('assets/common-js/scripts.js')}}"></script>
	<script>
        function deleteRecord($routeName,$reload){

            if(!confirm("Do you want to delete this ?")){
                return false;
            }

            if($reload == undefined){
                $reload = 3000;
            }

            $.post(
                $routeName,
                {
                    '_method':'DELETE',
                    '_token':$('meta[name="csrf-token"]').attr('content')
                },
                function(response){


                        $data = response;
                        if($data.status == true){
                            toastr.success($data.msg, 'Success !', {"closeButton": true});
                            if($reload){
                                setTimeout(function(){location.reload();},$reload);
                            }
                        }else{
                            toastr.error($data.msg, 'Error !', {"closeButton": true});
                        }

                }
            )
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
	</script>

	@yield('footer')
</body>
</html>
