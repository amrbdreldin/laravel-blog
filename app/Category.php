<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $dates = ['created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'title','user_id','image','slug'
    ];

    // relateion to get posts of category
    function posts(){
        return $this->hasMany('App\Post');
    }

    // relateion to get user who create ot update category
    function user(){
        return $this->belongsTo('App\User');
    }


}
