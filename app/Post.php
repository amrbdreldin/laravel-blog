<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $dates = ['created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'title', 'content','user_id','image','slug'
    ];

    // relateion to get comments of post
    function comments(){
        return $this->hasMany('App\Comment');
    }

    // relateion to get category of post
    function category(){
       return $this->belongsTo('App\Category');
   }

    // relateion to get user who create ot update post
    function user(){
        return $this->belongsTo('App\User');
    }


}
