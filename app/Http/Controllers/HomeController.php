<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        // Validate Data
        $request->validate([
            'word' => 'nullable|string',
            'slug' => 'nullable|exists:categories,slug',
        ]);


        $posts = Post::with(['category', 'comments'])->orderByRaw("RAND()");

        // filter posts by search word
        if ($request->word) {
            $posts->where('title', 'LIKE', '%' . $request->word . '%')
                ->orWhere('content', 'LIKE', '%' . $request->word . '%');
        }

        // filter posts by slug of category
        if ($request->slug) {
            $category = Category::where('slug', $request->slug)->first();
            if (!empty($category)) {
                $posts->where('category_id', $category->id);
            }
        }

        $data['posts'] = $posts->paginate(12);  // posts to show (it's better if select limit from setting :) )

        $data['categories'] = Category::limit(10)->get();  // categories to show (it's better if select limit from setting :) )

        return view('web.index', $data);
    }

    public function details(Request $request)
    {
            // check slug
        if (empty($request->slug)) {
            abort(404);
        }
        // check slug
        $post = Post::where('slug', $request->slug)->with(['category', 'comments'])->first();
        if (empty($post)) {
            abort(404);
        }

        $data['data'] = $post;
        $data['related'] = Post::where('category_id', $post->category_id)->where('id', '!=', $post->id)->limit(3)->get();
        $data['comments_count'] = $post->comments()->count();
        $data['comments'] = $post->comments()->orderBy('id', 'DESC')->limit(10)->get();
        $post->increment('views');  // we can use fingerprint or session to make increment is uniqe

        return view('web.details', $data);

    }

    public function comment(Request $request)
    {
        //validate data
        $request->validate([
            'name' => 'required|string',
            'comment' => 'required|string',
            'post_id' => 'required|int|exists:posts,id']);

        if($request->ajax()) {          // ignor other requests

            $store = Comment::create([
                'name' => $request->name,
                'comment' => $request->comment,
                'post_id' => $request->post_id
            ]);

            // check query
            if ($store) {
                return ['status' => true, 'msg' => 'Comment added'];
            } else {
                return ['status' => false, 'msg' => 'Please try again later'];
            }
        }else{
            abort('404');
        }
    }


}
