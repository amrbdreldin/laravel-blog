<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PostController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //validate Data
        $request->validate([
            'category_id' => 'nullable|int|exists:categories,id'
        ]);

        $data = Post::with(['user', 'category'])
            ->orderBy('id', 'DESC');

        if($request->category_id){
            $data->where('category_id',$request->category_id);
        }

        /***
         * it's better if make it server DataTable  :)
         * and make limit from setting :)
         */
        $data = $data->paginate(10);

        return view('admin.post.index', ['data' => $data]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::select('id','title')->get();
        return view('admin.post.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate Data
        $request->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'category_id' => 'required|int|exists:categories,id',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        // uniq name for image
        $imageName = time().uniqid() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('uploads/post'), $imageName);

        $store = Post::create([
            'title' => $request['title'],
            'content' => $request['content'],
            'category_id' => $request['category_id'],
            'user_id' => Auth::id(),
            'image' => $imageName
        ]);

        if ($store) {
            $store->update(['slug'=>slug($store->title,$store->id)]);
            return redirect()
                ->route('admin.post.create')
                ->with('status', 'success')
                ->with('msg', 'Data has been added successfully');
        } else {
            return redirect()
                ->route('admin.post.create')
                ->with('status', 'danger')
                ->with('msg', 'Sorry Couldn\'t add ');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::select('id','title')->get();
        return view('admin.post.create', ['data' => $post,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //validate data
        $validation = [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'category_id' => 'required|int|exists:categories,id'
        ];

        if ($request->image) {
            $validation['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|';
        }

        $request->validate($validation);
        if ($request->image) {
            // uniq name for image
            $imageName = time().uniqid() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('uploads/post'), $imageName);
        }

        $data = [
            'title' => $request['title'],
            'content' => $request['content'],
            'category_id' => $request['category_id'],
            'user_id' => Auth::id(),
            'slug' => slug($request['title'],$post->id)
        ];

        if ($request->image) {
            $data['image'] = $imageName;
        }

        $update = $post->update($data);

        //check query
        if ($update) {
            return redirect()
                ->route('admin.post.edit', $post->id)
                ->with('status', 'success')
                ->with('msg', 'Data has been Updated successfully');
        } else {
            return redirect()
                ->route('admin.post.edit', $post->id)
                ->with('status', 'danger')
                ->with('msg', 'Sorry Couldn\'t Updated');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {

        // delete comments of post
        $post->comments()->delete();

        // delete post
        if ($post->delete()) {
            return ['status' => true, 'msg' => __('News has been deleted successfully')];
        } else {
            return ['status' => false, 'msg' => 'Sorry Couldn\'t Delete'];
        }
    }

}
