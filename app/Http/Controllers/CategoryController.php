<?php

namespace App\Http\Controllers;

use App\Category;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CategoryController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /***
         * it's better if make it server DataTable  :)
         * and make limit from setting :)
         */
        $data = Category::with('user')
            ->orderBy('id', 'DESC')
            ->paginate(10);

        return view('admin.category.index', ['data' => $data]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate Data
        $request->validate([
            'title' => 'required|string|unique:categories|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|',
        ]);

        // uniq name for image
        $imageName = time() . uniqid() . '.' . request()->image->getClientOriginalExtension();
        request()->image->move(public_path('uploads/category'), $imageName);

        $store = Category::create([
            'title' => $request['title'],
            'user_id' => Auth::id(),
            'image' => $imageName
        ]);

        //check Query
        if ($store) {
            $store->update(['slug' => slug($request->title, $store->id)]);
            return redirect()
                ->route('admin.category.create')
                ->with('status', 'success')
                ->with('msg', 'Data has been added successfully');
        } else {
            return redirect()
                ->route('admin.category.create')
                ->with('status', 'danger')
                ->with('msg', 'Sorry Couldn\'t add ');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.create', ['data' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        // Validate Data
        $validation = [
            'title' => 'required|string|unique:categories,id,' . $category->id . '|max:255',
        ];

        if ($request->image) {
            $validation['image'] = 'required|image|mimes:jpeg,png,jpg,gif,svg|';
        }

        $request->validate($validation);
        if ($request->image) {
            // uniq name for image
            $imageName = time() . uniqid() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path('uploads/category'), $imageName);
        }

        $data = [
            'title' => $request['title'],
            'slug' => slug($request['title'], $category->id),
            'user_id' => Auth::id(),
        ];

        if ($request->image) {
            $data['image'] = $imageName;
        }

        $update = $category->update($data);

          //check Query
        if ($update) {
            return redirect()
                ->route('admin.category.edit', $category->id)
                ->with('status', 'success')
                ->with('msg', 'Data has been Updated successfully');
        } else {
            return redirect()
                ->route('admin.category.edit', $category->id)
                ->with('status', 'danger')
                ->with('msg', 'Sorry Couldn\'t Updated');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        // delete comments of posts of category
        if($category->posts->isNotEmpty()){
            foreach ($category->posts as $post){
                $post->comments()->delete();
            }
        }

        // delete posts of category
        $category->posts()->delete();

        // delete category
        if ($category->delete()) {
            return ['status' => true, 'msg' => __('News has been deleted successfully')];
        } else {
            return ['status' => false, 'msg' => 'Sorry Couldn\'t Delete'];
        }
    }
}
