<?php



/*
 * to cache form input error
 * paramerters
 * Object $error
 * String $filedName
 * Boolin $checkHasError
 */
function formError($error,$fieldName,$checkHasError = false){

    if($checkHasError){
        if($error->has($fieldName)){
            return ' has-danger';
        }else{
            return null;
        }
    }

    if($error->has($fieldName)){
        $return = '<p class="text-xs-left"><small class="danger text-muted">';

        foreach ($error->get($fieldName) as $errorMsg) {
            if(is_array($errorMsg)){
                $return .= implode(',',$errorMsg).'<br />';
            }else{
                $return .= $errorMsg.'<br />';
            }
        }
        $return .= '</small></p>';
        return $return;
    }else{
        return null;
    }

}


/*
 * Function to Create Sulg
 * Parameters
 * String $title
 * Int $id
 *
 *
 */

function slug($title,$id){

    $slug = str_replace(' ','-',$title);
    return $slug.'-'.$id;

}