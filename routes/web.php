<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/', 'HomeController@index')->name('home');
Route::get('/details/{slug}', 'HomeController@details')->name('details');
Route::post('/comment', 'HomeController@comment')->name('comment');

Route::group(['prefix'=>'admin'],function(){
    Auth::routes();

    Route::get('/','CategoryController@index');
    Route::resource('/category', 'CategoryController',['as'=>'admin']);
    Route::resource('/post', 'PostController',['as'=>'admin']);
    Route::get('logout', 'Auth\LoginController@logout', function () {
        return abort(404);
    })->name('logout');
});